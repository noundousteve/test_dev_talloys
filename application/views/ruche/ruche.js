function modal() {
    el = document.getElementById("backdrop1");
    el.style.display = (el.style.display == "block") ? "none" : "block";

    
    el1 = document.getElementById("modal1");
    el1.style.display = (el1.style.display == "block") ? "none" : "block";
    }

$(document).ready(function () {
    $('#ruches').DataTable({
        "language": {
            "lengthMenu": "Afficher _MENU_ enregistrements",
            "zeroRecords": "aucun résultat trouvé",
            "info": "Affichage des enregistrements de _START_ à _END_ sur un total de _TOTAL_ enregistrements",
            "infoEmpty": "Affichage des enregistrements de 0 à 0 sur un total de 0 enregistrements",
            "infoFiltered": "(filtrage de un total de _MAX_ enregistrements)",
            "sSearch": "Rechercher:",
            "oPaginate": {
                "sFirst": "Premier",
                "sLast": "Dernier",
                "sNext": "Suivant",
                "sPrevious": "Précédent"
            },
            "sProcessing": "En cours...",
        }
    });
});

