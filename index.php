<?php

require_once "tallyos/tallyos.php";

use tallyos\Configuration;
Configuration::setConfigurationFile('application/configuration.ini');
/**
 * en cas de modification des routes, mettre a jour l'autoloader
 * php composer.phar update
 */

define('APP_FOLDER', 'application');
define('TEMPLATE_FOLDER', APP_FOLDER . '/templates');
define('VIEW_FOLDER', APP_FOLDER . '/views');
define('RESOURCE_FOLDER', APP_FOLDER . '/resources');
define('APP_NAMESPACE', 'app');

$app = \tallyos\Application::getInstance();
$app->setdefaultControllerName('ruche');
$app->run();
