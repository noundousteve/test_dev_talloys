<?php
// recuperation du message en cas de success
use tallyos\Messenger;

$message = Messenger::getMessage();
if ($message !== false) :
    ?>
    <div class="alert alert-success" role="alert">
        <p><?php echo $message; ?></p>
    </div>

<?php endif; ?> 

 
 <!-- CSS personaliser --> 
 <link rel="stylesheet" href="application\views\ruche\ruche.scss">  

<div style="height: 50px;"></div>
<div class="container">
 
  <!-- Modal section -->
<div class="backdrop" id="backdrop1">

 <div class="modal" id="modal1" tabindex="-1" role="dialog" >

  <div class="modal-dialog modal-dialog-scrollable" role="document">

    <!-- Modal content-->

    <div class="modal-content">


      <div class="modal-header bg-dark">
        <h5 class="modal-title text-white">Création d'une nouvelle ruche</h5>
        <button type="button" class="close" aria-label="Close" onClick="modal()"><span
            aria-hidden="true">&times;</span></button>
      </div>

          <!-- model body section -->
          <div class="modal-body">
            <form method="post"> 
            <!-- recuperation du _formMessage en cas d' erreur --> 
              <?php if (isset($this->_formMessage) & $this->_formMessage != "") : ?>
                  <div class="alert alert-danger" role="alert">
                      <p><?php echo $this->_formMessage; ?></p>
                  </div>
              <?php endif; ?>

              <div class="form-group">
                <label for="Nom" class="col-form-label">Nom :</label>
                <input type="text" class="form-control" name="nom" id="nom" placeholder="Nom" value="<?php echo filter_var($this->nom, FILTER_SANITIZE_SPECIAL_CHARS); ?>">
              </div>

              <div class="form-group">
                <label for="latitude" class="col-form-label">Latitude :</label>
                <input type="number" step="any" class="form-control bs-datepicker"
                  name="latitude" id="latitude" placeholder="latitude" value="<?php echo filter_var($this->latitude, FILTER_SANITIZE_SPECIAL_CHARS); ?>">
              </div>

              
              <div class="form-group">
                <label for="longitude" class="col-form-label">Longitude :</label>
                <input type="number" step="any" class="form-control bs-datepicker"
                  name="longitude" id="longitude" placeholder="longitude" value="<?php echo filter_var($this->longitude, FILTER_SANITIZE_SPECIAL_CHARS); ?>">
              </div> 
              <hr>
              <button type="submit" name="submit" value="submit" class="btn btn-primary col-md-4 mr-5">
              <span class="mr-3"> Enregistrer </span>
              <i class="fa fa-save"></i>
            </button>
            <button type="button" class="btn btn-danger col-md-4 ml-5" onClick="modal()" data-dismiss="modal">
              <span class="mr-3"> Annuler </span>
              <i class="fa fa-close"></i>
            </button>
            </form>
          </div>

<!--
          <div class="modal-footer justify-content-center">

            <button type="submit" name="submit" value="submit" class="btn btn-primary col-md-4 mr-5">
              <span class="mr-3"> Enregistrer </span>
              <i class="fa fa-save"></i>
            </button>
            <button type="button" class="btn btn-danger col-md-4 ml-5" onClick="modal()" data-dismiss="modal">
              <span class="mr-3"> Annuler </span>
              <i class="fa fa-close"></i>
            </button>

          </div>

-->
        </div>
        <!-- /.modal-content -->

      </div>
      <!-- /.modal-dialog -->

    </div>
    <!-- /.modal !-->
  </div>  


<div class="row">
  <div class="col-lg-12">
  <div class="align-self-center">
  
        <button type="button" class="btn btn-success " onClick="modal()" data-dismiss="modal">
                    <span class="mr-3"> Ajouter une ruche </span>
                    <i class="fa fa-plus"></i>
                  </button> 
  </div>
            <p></p>

    <div class="table-responsive">
      <table id="ruches" class="table table-striped table-bordered" style="width:100%">
        <thead>
          <tr>
            <th>Nom</th>
            <th>Latitude</th>
            <th>Longitude</th>
            <th></th> 
          </tr>
        </thead> 
        <tbody>
                <?php foreach ($this->ruches as $m) : ?>
                    <tr>

                        <td>
                            <?php echo utf8_encode($m["nom"]) ?>
                        </td>
                        <td><?php echo $m["latitude"] ?></td>
                        <td><?php echo $m["longitude"] ?></td>
                        <td>
                                <a  href="?controller=ruche&action=modifier&idRuche=<?php echo  $m["id_ruche"] ?>&nom=<?php echo  $m["nom"] ?>&longitude=<?php echo  $m["longitude"] ?>&latitude=<?php echo  $m["latitude"] ?>">
                                    Modifier
                                </a> /
                                <a href="?controller=ruche&action=suprimer&idRuche=<?php echo  $m["id_ruche"] ?>">
                                    Suprimer
                                </a>
                        </td>  
                    </tr>
                <?php endforeach; ?>
            </tbody>
        <tfoot>
          <tr>
            <th>Nom</th>
            <th>Latitude</th>
            <th>Longitude</th>
            <th></th> 
          </tr>
          </tr>
        </tfoot>
      </table> 
    </div>
  </div>
</div>
</div>

<!-- JS personaliser --> 
<script type="text/javascript" src="application\views\ruche\ruche.js" defer></script>

