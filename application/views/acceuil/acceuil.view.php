 <!-- CSS personaliser --> 
 <link rel="stylesheet" href="application\views\acceuil\acceuil.scss">  

<div style="height: 50px;"></div>

<div class="container">
    <div class="row">
        <div class="col-lg-9"> 
            <div class="row">
                <div class="col-lg-3">
                <img src="https://sofes.miximages.com/html/320x200.png" class="rounded-circle" alt="Cinque Terre">
                </div>
                <div class="col-lg-9"> 
                    <form>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Template name</label>
                                <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
                                <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Subject</label>
                                <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
                                <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                            </div>
                    </form>
                    </div>

            </div>  
                <div class="form-group">
                    <label for="exampleFormControlTextarea1">Example textarea</label>
                    <textarea class="form-control" id="exampleFormControlTextarea1" rows="5"></textarea>
                </div>  

        </div>

        <div class="col-lg-3">
                    <div class="card" style="width: 18rem;">
            <img class="card-img-top" src="https://sofes.miximages.com/html/320x200.png" alt="Card image cap">
            <div class="card-body">
                <h5 class="card-title">Card title</h5>
                <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                <a href="#" class="btn btn-primary">Button</a>
                <a href="#" class="btn btn-light">Button</a>
            </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-3"> 
            <div class="form-group">
                <label for="sel1">Message Type:</label>
                <select class="form-control" id="sel1">
                    <option value="" disabled selected>Mail + Push</option>
                    <option value="1">Option 1</option>
                    <option value="2">Option 2</option>
                    <option value="3">Option 3</option>
                </select>
                </div>
                
            <label for="sel1">Send to Group</label>
            <!-- Default checked -->
            <div class="custom-control custom-checkbox">
            <input type="checkbox" class="custom-control-input" id="defaultChecked2" checked>
            <label class="custom-control-label" for="defaultChecked2">Top Management</label>
            </div>
            <!-- Default unchecked -->
            <div class="custom-control custom-checkbox">
                <input type="checkbox" class="custom-control-input" id="defaultUnchecked">
                <label class="custom-control-label" for="defaultUnchecked">Marketing Department</label>
            </div>
            <!-- Default checked -->
            <div class="custom-control custom-checkbox">
            <input type="checkbox" class="custom-control-input" id="defaultChecked2" checked>
            <label class="custom-control-label" for="defaultChecked2">Desing Department</label>
            </div>
            <!-- Default unchecked -->
            <div class="custom-control custom-checkbox">
                <input type="checkbox" class="custom-control-input" id="defaultUnchecked">
                <label class="custom-control-label" for="defaultUnchecked">Finacial Department</label>
            </div>
            <!-- Default unchecked -->
            <div class="custom-control custom-checkbox">
                <input type="checkbox" class="custom-control-input" id="defaultUnchecked">
                <label class="custom-control-label" for="defaultUnchecked">Supply Departement</label>
            </div>
            <br>
            <div class="row"> 
                <a href="#" class="btn btn-primary">Valider</a> 
                <a href="#" class="btn btn-light">Annuler</a>
                </div>

        </div>

        <div class="col-lg-6">  
        </div>
        
        <div class="col-lg-3"> 
            <div class="form-group">
            <label for="sel1">Tap Target:</label>
            <select class="form-control" id="sel1">
                <option value="" disabled selected>Profile Screen</option>
                <option value="1">Option 1</option>
                <option value="2">Option 2</option>
                <option value="3">Option 3</option>
            </select>
            </div> 
            <label for="sel1">Set Type</label>
            <div class="form-check">
            <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios2" value="option2">
            <label class="form-check-label btn-success" for="exampleRadios2">
                News 
            </label>
            </div>
            <div class="form-check">
                <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios1" value="option1" checked>
                <label class="form-check-label btn-primary" for="exampleRadios1">
                    Repports
                </label>
            </div>

            <div class="form-check">
            <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios2" value="option2">
            <label class="form-check-label btn-warning" for="exampleRadios2">
                Documents
            </label>
            </div>

            <div class="form-check">
            <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios2" value="option2">
            <label class="form-check-label btn-info" for="exampleRadios2">
                media
            </label>
            </div>

            <div class="form-check">
            <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios2" value="option2">
            <label class="form-check-label btn-secondary" for="exampleRadios2">
                test
            </label>
            </div>
            <br>
            <a href="#" class="btn btn-danger">Supprimer</a>
        </div>
    </div>

</div>

<!-- JS personaliser --> 
<script type="text/javascript" src="application\views\acceuil\acceuil.js" defer></script>

