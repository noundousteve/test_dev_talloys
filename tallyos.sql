-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jun 22, 2020 at 06:50 PM
-- Server version: 5.7.24
-- PHP Version: 7.2.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tallyos`
--

-- --------------------------------------------------------

--
-- Table structure for table `ruche`
--

DROP TABLE IF EXISTS `ruche`;
CREATE TABLE IF NOT EXISTS `ruche` (
  `id_ruche` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) NOT NULL,
  `latitude` float NOT NULL,
  `longitude` float NOT NULL,
  PRIMARY KEY (`id_ruche`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ruche`
--

INSERT INTO `ruche` (`id_ruche`, `nom`, `latitude`, `longitude`) VALUES
(1, 'Ruche A', 49.108, 6.17038),
(2, 'Ruche B', 49.2954, 6.17038);


-- --------------------------------------------------------

--
-- Table structure for table `information`
--

DROP TABLE IF EXISTS `information`;
CREATE TABLE IF NOT EXISTS `information` (
  `id_information` int(11) NOT NULL AUTO_INCREMENT,
  `id_ruche` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `poids` float NOT NULL,
  `temperature` int(11) NOT NULL,
  `humidite` int(11) NOT NULL,
  PRIMARY KEY (`id_information`),
  KEY `FK_information_ruche` (`id_ruche`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `information`
--

INSERT INTO `information` (`id_information`, `id_ruche`, `date`, `poids`, `temperature`, `humidite`) VALUES
(1, 1, '2020-06-01 18:30:00', 44.5, 15, 79),
(2, 1, '2020-06-01 19:00:00', 45, 16, 77);


-- --------------------------------------------------------

--
-- Stand-in structure for view `v_information`
-- (See below for the actual view)
--
DROP VIEW IF EXISTS `v_information`;
CREATE TABLE IF NOT EXISTS `v_information` (
`nom` varchar(255)
,`date` datetime
,`poids` float
,`temperature` int(11)
,`humidite` int(11)
);

-- --------------------------------------------------------

--
-- Structure for view `v_information`
--
DROP TABLE IF EXISTS `v_information`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_information`  AS  select `r`.`nom` AS `nom`,`i`.`date` AS `date`,`i`.`poids` AS `poids`,`i`.`temperature` AS `temperature`,`i`.`humidite` AS `humidite` from (`ruche` `r` join `information` `i`) where (`i`.`id_ruche` = `r`.`id_ruche`) ;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `information`
--
ALTER TABLE `information`
  ADD CONSTRAINT `FK_information_ruche` FOREIGN KEY (`id_ruche`) REFERENCES `ruche` (`id_ruche`)  ON DELETE CASCADE
      ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
