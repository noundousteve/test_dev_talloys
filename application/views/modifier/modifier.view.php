
<div class="container">

    <div class="row">
        <div class="col-12">
            <div class="card" style="padding: 2rem;margin:2rem;">
                <form method="post"> 
                <!-- recuperation du _formMessage en cas d' erreur --> 
                <?php if (isset($this->_formMessage) & $this->_formMessage != "") : ?>
                    <div class="alert alert-danger" role="alert">
                        <p><?php echo $this->_formMessage; ?></p>
                    </div>
                <?php endif; ?>

                <div class="form-group">
                    <label for="Nom" class="col-form-label">Nom :</label>
                    <input type="text" class="form-control" name="nom" id="nom" placeholder="Nom" value="<?php echo filter_var($this->nom, FILTER_SANITIZE_SPECIAL_CHARS); ?>">
                </div>

                <div class="form-group">
                    <label for="latitude" class="col-form-label">Latitude :</label>
                    <input type="number" step="any" class="form-control bs-datepicker"
                    name="latitude" id="latitude" placeholder="latitude" value="<?php echo filter_var($this->latitude, FILTER_SANITIZE_SPECIAL_CHARS); ?>">
                </div>

                
                <div class="form-group">
                    <label for="longitude" class="col-form-label">Longitude :</label>
                    <input type="number" step="any" class="form-control bs-datepicker"
                    name="longitude" id="longitude" placeholder="longitude" value="<?php echo filter_var($this->longitude, FILTER_SANITIZE_SPECIAL_CHARS); ?>">
                </div> 
                <hr>
                <button type="submit" name="submit" value="submit" class="btn btn-primary col-md-4 mr-5">
                <span class="mr-3"> modifier </span>
                <i class="fa fa-save"></i>
                </button>   
                <a href="?controller=ruche&action=lister" class="btn btn-danger col-md-4 mr-5">Annuler <i class="fa fa-close"></i> </a>
                
                </form>
            </div>
        
        </di>
    </div>
</div>