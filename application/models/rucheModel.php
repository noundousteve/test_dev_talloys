<?php

namespace app\models;

use tallyos\Error;

class rucheModel
{
    // recuperer les ruches
    public static function getAllRuche()
    {
        $pdo = \tallyos\Database::getInstance();

        $req = $pdo->prepare("SELECT * FROM ruche");

        try { 
            $req->execute();
            $data = $req->fetchAll();
        } catch (\PDOException $ex) {
            die("Erreur SQL " . $ex->getMessage());
        }

        return $data;
    }

    // recuperer les information des ruches
    public static function getAllinformation()
    {
        $pdo = \tallyos\Database::getInstance();

        $req = $pdo->prepare("SELECT * FROM v_information");

        try { 
            $req->execute();
            $data = $req->fetchAll();
        } catch (\PDOException $ex) {
            die("Erreur SQL " . $ex->getMessage());
        }

        return $data;
    }

    public static function insertRuche(array $data)
    {
        $expectedKeys = ['nom', 'latitude', 'longitude'];
        foreach ($expectedKeys as $key) {
            if (!isset($data[$key])) {
                throw new Error("$key manquant");
            }
        }
        $pdo = \tallyos\Database::getInstance();
        print_r($data);

        $req = $pdo->prepare("INSERT INTO ruche(nom,latitude,longitude) 
        VALUES (:nom,:latitude,:longitude)");

        try {
            $req->bindValue(':nom', $data['nom']);
            $req->bindValue(':latitude', $data['latitude']);
            $req->bindValue(':longitude', $data['longitude']); 
            $req->execute();
        } catch (\PDOException $ex) {
            die("Erreur SQL " . $ex->getMessage());
        }
        return $pdo->lastInsertId();
    }
     
    // modification d'une ruche
    public static function updateRuche(array $data)
        {
            
            $expectedKeys = ['nom', 'latitude', 'longitude', 'id_ruche'];
            foreach ($expectedKeys as $key) {
                if (!isset($data[$key])) {
                    throw new Error("$key manquant");
                }
            }
            $pdo = \tallyos\Database::getInstance();
    
            $req = $pdo->prepare("UPDATE `ruche` SET `nom`= :nom, latitude = :latitude, longitude = :longitude WHERE id_ruche = :id_ruche");

            try {
                $req->bindValue(':nom',$data['nom']);
                $req->bindValue(':latitude', $data['latitude']);
                $req->bindValue(':longitude', $data['longitude']);
                $req->bindValue(':id_ruche', $data['id_ruche']);
                $req->execute();
            } catch (\PDOException $ex) {
                die("Erreur SQL " . $ex->getMessage());
            }
            return $pdo->lastInsertId();
        }

         
    // suprimer une ruche
    public static function deleteRuche($idRuche)
    {
        $pdo = \tallyos\Database::getInstance();

        $req = $pdo->prepare("DELETE FROM `ruche` WHERE id_ruche = :idRuche");

        try {
            $req->bindValue(':idRuche', $idRuche);
            $req->execute();
        } catch (\PDOException $ex) {
            die("Erreur SQL " . $ex->getMessage());
        }
        return $pdo->lastInsertId();
    }

    
}
