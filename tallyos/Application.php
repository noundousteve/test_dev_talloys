<?php

namespace tallyos;

use Monolog\Handler\StreamHandler;
use Monolog\Logger;

class Application
{
    public const DEBUG_MODE = 'debug';
    public const PRODUCTION_MODE = 'production';

    private $runMode = self::PRODUCTION_MODE;

    private static $_instance = null;
    private $defaultControllerName = null;
    private $logger = null;

    private function __construct()
    {
        $this->startLogger();
        $this->setRunMode();
    }

      // création d'une instance de l'application
      public static function getInstance()
      {
          if (is_null(self::$_instance)) {
              self::$_instance = new Application();
          }
          return self::$_instance;
      }

     // recupération de la class d'un controller
    private function getControllerClass($controllerName)
    {
        if ($controllerName === 'error') {
            return 'tallyos\\errorController';
        }

        $controllerClass = APP_NAMESPACE . '\Controllers\\' . $controllerName . 'Controller';

        if (!class_exists($controllerClass)) {
            throw new Error('Application : controleur introubable ' . $controllerClass);
        }
        return $controllerClass;
    }

    // controler par défaut 
    public function setdefaultControllerName($controllerName)
    {
        $this->defaultControllerName = $controllerName;
    }

     // lancement de l'application
     public function run()
     {
         try {
             // Récuperation dans GET du paramétre controller
 
             $controllerName = filter_input(INPUT_GET, 'controller');
             if (is_null($controllerName)) {
                 if (is_null($this->defaultControllerName)) {
                     throw new Error('Application : aucun controleur renseigné');
                 } else {
                     $controllerName = $this->defaultControllerName;
                 }
             }
 
             // Génération du nom de la classe
 
             $controllerClass = $this->getControllerClass($controllerName);
 
             // Construction de l'objet
             $controller = new $controllerClass();
 
             // Récuperation de l'action à exécuter
             $actionName = filter_input(INPUT_GET, 'action');
 
             if (is_null($actionName)) {
                 
                 $actionName = $controller->getDefaultActionName();
             }
 
             // Exécution de l'action demandée
             $controller->execute($actionName);
 
             // Rendu de la page
             Page::getInstance()->render();
  
 
         } catch (Error $exp) {
             $exp->render();
         } catch (\Exception $exp) {
             $this->logger->addError($exp->getMessage());
             if ($this->runMode == self::DEBUG_MODE) {
                 throw $exp;
             } else {
                 self::redirect('?controller=error');
             }
         }
     }

    /**
     * verifie si la configuration par défault a été renseigner dans le fichier de configuration
     * démarage en mode production dans le cas contraire
     */
    private function setRunMode()
    {
        // création d'un instance de la class configuration
        $conf = Configuration::getInstance();

        if (isset($conf->run_mode) && $conf->run_mode === self::DEBUG_MODE) {
            $this->runMode = self::DEBUG_MODE;
            \error_reporting(E_ALL);
        } else {
            \error_reporting(0);
        }
    }

    // retourne le mode à  utiliser
    public function getRunMode()
    {
        return $this->runMode;
    }

    // function de gestion de log
    private function startLogger()
    {
        $this->logger = new Logger('tallyos');
        $this->logger->pushHandler(new StreamHandler('log/app.log'), Logger::DEBUG);
        $this->logger->info('App started');
    }

    // recupération des logs
    public function getLogger()
    {
        return $this->logger;
    }

   // fonction de redirection
    public static function redirect($url)
    {
        if (!headers_sent()) {
            header("HTTP/1.0 303 See Other");
            header("Location: " . $url);
            die();
        } else {
            ?>
            <script>
                window.location = '<?php echo $url; ?>';
            </script>
<?php
        }
    }
}
