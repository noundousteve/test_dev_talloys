<?php

namespace app\modules;

class Footer implements \tallyos\Module
{ 
    public function render()
    {
        ?>
    
    <div class="row justify-content-center">  
            <img class="mb-2" src="../../assets/brand/bootstrap-solid.svg" alt="" width="24" height="24">
          <div class="col-6 col-md">
            <h5>tallyos</h5>
            <ul class="list-unstyled text-small"> 
                            <li><a href="#" title="Fonctionnalités">Fonctionnalités</a></li>
                            <li><a href="#" title="Tarifs">Tarifs</a></li>
                            <li><a href="#" title="Connexion">Connexion</a></li>
							<li><a href="#" target="_blank" title="tallyos et le RGPD">RGPD</a></li>
                        
            </ul>
          </div>

          <div class="col-6 col-md">
            <h5>Communauté</h5>
            <ul class="list-unstyled text-small"> 
                            <li><a href="#" target="_blank" title="Blog">Blog</a></li>
                            <li><a href="#" target="_blank" title="Youtube">Youtube</a></li>
                            <li><a href="#" target="_blank" title="Twitter">Twitter</a></li>
							<li><a href="#" target="_blank" title="Newsletter">Newsletter</a></li>
                        </ul>
          </div>

          <div class="col-6 col-md">
            <h5>à propos</h5>
            <ul class="list-unstyled text-small"> 
                            <li><a href="#" target="_blank" title="Plebiscit">toto</a></li>
                            <li><a href="#" target="_blank" title="CGU - CGV">CGU - CGV</a></li>
                            <li><a href="#" data-featherlight="#legal-msg"  title="Mentions légales">Mentions légales</a></li>
							<li><a href="#" target="_blank" title="Contactez nous">Contactez nous</a></li>
                        </ul>
          </div>

          
          <div class="col-6 col-md">
            <h5>tallyos</h5>
            <ul class="list-unstyled text-small"> 
                            <li class="footer-coord">Un service édité par toto<br> Lorm 185 rue de la dfs - BP 92 881<br>1610 LA toto Cedex 1</li>
							<li><a href="#">hello@tallyos.com</a></li>
             </ul>
          </div>

        </div>

        <div class="row">
        <div class="col-12 col-md">
            <small class="d-block mb-3 text-center">Copyright tallyos 2019 ©</small>
            
          </div>
        </div>
<?php
    }
}
