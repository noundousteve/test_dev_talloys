<?php
namespace app\tests; 

//use garage\Configuration; 
//Configuration::setConfigurationFile('application/configuration.ini');  

use app\models\RucheModel;
use tallyos\Configuration;


  class RucheTest extends \PHPUnit\Framework\TestCase{

    use \PHPUnit\DbUnit\TestCaseTrait;
    
    // only instantiate pdo once for test clean-up/fixture load
    static private $pdo = null;

    // only instantiate PHPUnit\DbUnit\Database\Connection once per test
    private $conn = null;

    public function __construct()
    {  
        //Configuration::setConfigurationFile('application/configuration.ini');
    }

    /**
     * @return PHPUnit\DbUnit\Database\Connection
     */
    final public function getConnection()
    {
        if ($this->conn === null) {
            if (self::$pdo == null) {
                self::$pdo = new \PDO( $GLOBALS['DB_DSN'], $GLOBALS['DB_USER'], $GLOBALS['DB_PASSWD'] );
            }
            $this->conn = $this->createDefaultDBConnection(self::$pdo, $GLOBALS['DB_DBNAME']);
        }

        return $this->conn;
    }

    /**
     * @return PHPUnit\DbUnit\DataSet\IDataSet
     */ 
    
    public function getDataSet()
    {
        return $this->createMySQLXMLDataSet('application/tests/tallyos.xml');
    }

    public function testCalculate()
    {
        $this->assertSame(2, 1 + 1);
    }

 
}