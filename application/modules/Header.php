<?php

namespace app\modules;

class Header implements \tallyos\Module
{ 
    private static $items = [
        [
            "title" => "Acceuil",
            "url" => "?controller=ruche&action=acceuil" 
        ],
        [
            "title" => "Ruches",
            "url" => "?controller=ruche&action=lister" 
        ],
        [
            "title" => "Informations",
            "url" => "?controller=ruche&action=information" 
        ]
    ];
    public function render()
    { 
        ?>
                 
        <nav class="navbar navbar-expand-lg navbar-dark ">
            
            <a class="navbar-brand" href="#" title="tallyos">
           <!-- <img src="https://www.tallyos.com/img/tallyos.png" alt="tallyos"> -->
                <span class="navbar-text">
                    Company
                </span>
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarText">
                <ul class="navbar-nav mr-auto" id="navigation">

                    <?php foreach (self::$items as $M) : ?>
                        <li class="nav-item">
                            <a class="nav-link" href="<?php echo $M['url']; ?>">
                                <?php echo $M['title']; ?>
                            </a>
                        </li>
                    <?php endforeach; ?>
                </ul>
                <span class="navbar-text">
                    Déconnexion
                </span>
            </div>
        </nav>
                
 
<?php
    }
}
