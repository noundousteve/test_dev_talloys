<?php

namespace tallyos;

interface Module
{
    // constructeur par défaut d'un Module
    public function render();
}
