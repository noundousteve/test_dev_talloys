<?php

namespace app\controllers;

use tallyos\Controller;
use tallyos\Page;
use app\models\rucheModel;
use tallyos\Application;
use tallyos\Messenger;
use tallyos\Error;

class rucheController extends Controller
{

    public function getDefaultActionName()
    {
        return 'acceuil';
    }


    public function listerAction()
    {

        $page = Page::getInstance();
        $page->_formMessage = "";
        $page->init('ruche', 'ruche');

         // Préparation des données
         $page->_formMessage = "";  
         $page->nom = "";
         $page->latitude = ""; 
         $page->longitude = ""; 


        $page->ruches = rucheModel::getAllRuche();
      
        // si le formulaire n'est pas envoyé
        if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
            return;
        }
        
        $page->nom = filter_input(INPUT_POST, 'nom');
        if (trim($page->nom) === "") {
            $page->_formMessage = "Erreur : Veuillez remplir le champ nom";
            return;
        }

        $page->latitude = filter_input(INPUT_POST, 'latitude');
        if (trim($page->latitude) === "" || trim($page->latitude) <= 0 ){
            $page->_formMessage = "Erreur : Veuillez remplir le champ latitude";
            return;
        }

        $page->longitude = filter_input(INPUT_POST, 'longitude');
        if (trim($page->longitude) === "" || trim($page->longitude) <= 0 ) {
            $page->_formMessage = "Erreur : Veuillez remplir le champ longitude";
            return;
        }
 

        
        // Enregistrement  
        rucheModel::insertRuche([
            'nom' => $page->nom,
            'latitude' => $page->latitude,
            'longitude' => $page->longitude
        ]);
        Messenger::setMessage("Ruche enregistrer avec succès !!!");
        Application::redirect('?controller=ruche&action=lister');
    }

    public function informationAction()
    {

        $page = Page::getInstance();
        $page->_formMessage = "";
        $page->init('ruche', 'information');
        $page->informations = rucheModel::getAllinformation();
    }

    public function acceuilAction()
    {

        $page = Page::getInstance();
        $page->_formMessage = "";
        $page->init('ruche', 'acceuil'); 
    }

    public function modifierAction()
    {

        $page = Page::getInstance();
        $page->_formMessage = "";
        $page->init('ruche', 'modifier');

        $page->idRuche = filter_input(INPUT_GET, 'idRuche');
        $page->nom = filter_input(INPUT_GET, 'nom');
        $page->latitude = filter_input(INPUT_GET, 'latitude');
        $page->longitude = filter_input(INPUT_GET, 'longitude');

           // si le formulaire n'est pas envoyé
           if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
            return;
        }

        $page->nom = filter_input(INPUT_POST, 'nom');
        if (trim($page->nom) === "") {
            $page->_formMessage = "Erreur : Veuillez remplir le champ nom";
            return;
        }

        $page->latitude = filter_input(INPUT_POST, 'latitude');
        if (trim($page->latitude) === "" || trim($page->latitude) <= 0 ){
            $page->_formMessage = "Erreur : Veuillez remplir le champ latitude";
            return;
        }

        $page->longitude = filter_input(INPUT_POST, 'longitude');
        if (trim($page->longitude) === "" || trim($page->longitude) <= 0 ) {
            $page->_formMessage = "Erreur : Veuillez remplir le champ longitude";
            return;
        }
        
        // Enregistrement  
        rucheModel::updateRuche([
            'id_ruche' => $page->idRuche,
            'nom' => $page->nom,
            'latitude' => $page->latitude,
            'longitude' => $page->longitude
        ]);
        Messenger::setMessage("Ruche mise a jour avec succès !!!");
        Application::redirect('?controller=ruche&action=lister');
 
    }

    public function suprimerAction()
    {

        $page = Page::getInstance();
        $page->init('ruche', 'ruche');

        $page->idRuche = filter_input(INPUT_GET, 'idRuche');

        if (is_null($page->idRuche)) {
            $page->_formMessage = "Erreur : imposible d'effectuer l'operation ";
            return;
        }

        // Enregistrement 
        rucheModel::deleteRuche($page->idRuche);
        Messenger::setMessage("La ruche a été suprimer");
        Application::redirect('?controller=ruche&action=lister');
    }
 
}
