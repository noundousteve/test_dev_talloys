 <!-- CSS personaliser --> 
 <link rel="stylesheet" href="application\views\information\information.scss">  

<div style="height: 50px;"></div>
<div class="container">
<div class="row">
  <div class="col-lg-12">
  <div class="align-self-center">
                <h3>Information Ruche</h3>
            </div>
    <div class="table-responsive">
      <table id="informations" class="table table-striped table-bordered" style="width:100%">
        <thead>
          <tr>
            <th>Ruche</th>
            <th>Date</th>
            <th>Poids</th>
            <th>Température</th>
            <th>Humidité</th> 
          </tr>
        </thead> 
        <tbody>
                <?php foreach ($this->informations as $m) : ?>
                    <tr> 
                        <td><?php echo $m["nom"] ?></td>
                        <td><?php echo $m["date"] ?></td> 
                        <td><?php echo $m["poids"] ?></td>
                        <td><?php echo $m["temperature"] ?></td> 
                        <td><?php echo $m["humidite"] ?></td> 
                    </tr>
                <?php endforeach; ?>
            </tbody>
        <tfoot>
          <tr>
            <th>Ruche</th>
            <th>Date</th>
            <th>Poids</th>
            <th>Température</th>
            <th>Humidité</th> 
          </tr>
          </tr>
        </tfoot>
      </table> 
    </div>
  </div>
</div>
</div>

<!-- JS personaliser --> 
<script type="text/javascript" src="application\views\information\information.js" defer></script>

